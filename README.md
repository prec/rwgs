# RWGS
The code in this repository is used to calculate and illustrate the thermodynamic limits of the reverse water gas shift peerformed via a conventional cofeed catalytic reactor and in a countercurrent oxygen permeable membrane reactor.

# Dependencies
The following python packages are required:

cantera

numpy

matplotlib

